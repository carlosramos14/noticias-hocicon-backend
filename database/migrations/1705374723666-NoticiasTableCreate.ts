import { MigrationInterface, QueryRunner } from "typeorm";

export class NoticiasTableCreate1705374723666 implements MigrationInterface {
    name = 'NoticiasTableCreate1705374723666'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "noticias" ("id" SERIAL NOT NULL, "titulo" character varying NOT NULL, "fechaPublicacion" date NOT NULL DEFAULT now(), "lugar" character varying NOT NULL, "autor" character varying NOT NULL, "contenido" character varying NOT NULL, CONSTRAINT "UQ_28e70bb68b6fd6dbf29de5874f0" UNIQUE ("titulo"), CONSTRAINT "PK_526a107301fc9dfe8d836d6cf27" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "noticias"`);
    }

}
