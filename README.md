<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>
# BACKEND

# Portal de noticias "EL HOCICON" 

## Tecnologías utilizadas

- NodeJS versión 20
- NestJS versión 10
- TypeORM versión 0.3
- Postgresql versión 16

## Instalación

Descargar del Repositorio

```bash
$ git clone https://gitlab.com/carlosramos14/noticias-hocicon-backend.git
```

Ir a la carpeta del proyecto descargada

```bash
$ cd noticias-hocicon-backend/
```

Instalar el nestjs y sus dependencias

```bash
$ npm install
```

## Crear la Base de datos

Antes de hacer correr el proyecto (levantar el servidor web), se debe crear la base de datos "hocicondb"

Se puede hacer mediante algún cliente gráfico o terminal.

Ejemplo por terminal:

```bash
CREATE DATABASE hocicondb;
\c hocicondb;
```

## Ejecutar las migraciones

Desde la terminal, en la carpeta raíz del proyecto, ejecutar

```bash
$ npm run migration:run
```

Esto creará las tablas en la base de datos.

## Ejecutar los seeders

1. Modificar el archivo `database/datasource.js`, la línea entities debe quedar así:

   ```
   entities: ['src/**/*.entity{.ts,.js}'],
   ```

   

2. Ejecutar el comando para cargar los seeders

   ```
   npm seed:run
   ```

   

## Running the app

Devolver como estaba el archivo `database/datasource.js`, debe quedar así

```typescript
entities: ['dist/**/*.entity{.ts,.js}'],
```

Luego ejecutar el comando:

```bash
# development
$ npm run start
```

La aplicación se ejecutará en el **puerto 3333**

## Las rutas de la API

```
todas las noticias
GET http://localhost:3333/noticias

Una noticia 
GET http://localhost:3333/noticias/1

Crear una noticia
POST http://localhost:3333/noticias

Modificar una noticia
PUT http://localhost:3333/noticias/1

Eliminar una noticia
DELETE http://localhost:3333/noticias/1

```



---

Postulante: Carlos Ramos
