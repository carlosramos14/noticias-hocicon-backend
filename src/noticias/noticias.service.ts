import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Noticia } from './noticias.entity';
import { CreateNoticiaDto, UpdateNoticiaDto } from './dto/noticias.dto';
import { Repository } from 'typeorm';
import { HttpException } from '@nestjs/common';

@Injectable()
export class NoticiasService {
  private noticias: Noticia[] = [];

  constructor(
    @InjectRepository(Noticia) private noticiaRepository: Repository<Noticia>,
  ) {}

  getAllNoticias() {
    return this.noticiaRepository.find();
  }

  async getNoticia(id: number) {
    const noticiaFound = await this.noticiaRepository.findOne({
      where: {
        id: id,
      },
    });

    if (!noticiaFound) {
      return new HttpException('Noticia no existe', HttpStatus.NOT_FOUND);
    }

    return noticiaFound;
  }

  async createNoticia(newNoticia: CreateNoticiaDto) {
    const noticiaFound = await this.noticiaRepository.findOne({
      where: {
        titulo: newNoticia.titulo,
      },
    });

    if (noticiaFound) {
      return new HttpException('Noticia ya existe', HttpStatus.CONFLICT);
    }

    return this.noticiaRepository.save(newNoticia);
  }

  async deleteNoticia(id: number) {
    const noticiaFound = await this.noticiaRepository.findOne({
      where: {
        id: id,
      },
    });

    if (!noticiaFound) {
      return new HttpException('Noticia no existe', HttpStatus.NOT_FOUND);
    }

    return this.noticiaRepository.delete({ id });
  }

  async updateNoticia(id: number, updateFields: UpdateNoticiaDto) {
    const noticiaFound = await this.noticiaRepository.findOne({
      where: {
        id: id,
      },
    });

    if (!noticiaFound) {
      return new HttpException('Noticia no existe', HttpStatus.NOT_FOUND);
    }

    const noticiaTituloRepetido = await this.noticiaRepository.findOne({
      where: {
        titulo: updateFields.titulo,
      },
    });

    if (noticiaTituloRepetido) {
      return new HttpException('El título está repetido', HttpStatus.CONFLICT);
    }

    return this.noticiaRepository.update({ id }, updateFields);
    // const noticia = this.noticias.find((noticia) => noticia.id == id);
    // if (noticia !== undefined) {
    //   //delete object properties that are undefined
    //   Object.keys(updateFields).forEach(
    //     (key) => updateFields[key] === undefined && delete updateFields[key],
    //   );

    //   const newNoticia = Object.assign(noticia, updateFields);
    //   this.noticias = this.noticias.map((noticia) =>
    //     noticia.id == id ? newNoticia : noticia,
    //   );
    // }
    // return noticia;
  }
}
