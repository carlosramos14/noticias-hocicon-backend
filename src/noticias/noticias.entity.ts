import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('noticias')
export class Noticia {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  titulo: string;

  @Column({ type: 'date', default: () => 'CURRENT_TIMESTAMP' })
  fechaPublicacion: Date;

  @Column()
  lugar: string;

  @Column()
  autor: string;

  @Column()
  contenido: string;
}
