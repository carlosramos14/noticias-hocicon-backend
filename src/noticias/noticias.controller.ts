import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { CreateNoticiaDto, UpdateNoticiaDto } from './dto/noticias.dto';
import { Noticia } from './noticias.entity';

@Controller('noticias')
export class NoticiasController {
  constructor(private noticiasService: NoticiasService) {}

  @Get()
  getAllNoticias(): Promise<Noticia[]> {
    return this.noticiasService.getAllNoticias();
  }

  @Get(':id')
  getNoticia(@Param('id', ParseIntPipe) id: number) {
    return this.noticiasService.getNoticia(id);
  }

  @Post()
  createNoticia(@Body() newNoticia: CreateNoticiaDto) {
    return this.noticiasService.createNoticia({
      titulo: newNoticia.titulo,
      fechaPublicacion: newNoticia.fechaPublicacion,
      lugar: newNoticia.lugar,
      autor: newNoticia.autor,
      contenido: newNoticia.contenido,
    });
  }

  @Delete(':id')
  deleteNoticia(@Param('id', ParseIntPipe) id: number) {
    return this.noticiasService.deleteNoticia(id);
  }

  @Put(':id')
  updateNoticia(
    @Param('id') id: number,
    @Body() updateFields: UpdateNoticiaDto,
  ) {
    const noticia = this.noticiasService.updateNoticia(id, {
      titulo: updateFields.titulo,
      fechaPublicacion: updateFields.fechaPublicacion,
      lugar: updateFields.lugar,
      autor: updateFields.autor,
      contenido: updateFields.contenido,
    });
    //return the updated noticia or if noticia is undefined return a code 404
    return (
      noticia || { statusCode: 404, message: 'Noticia {' + id + '} not found' }
    );
  }
}
