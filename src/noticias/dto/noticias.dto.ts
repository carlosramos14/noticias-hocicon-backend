import {
  IsString,
  IsNotEmpty,
  IsOptional,
  IsDateString,
} from 'class-validator';

export class CreateNoticiaDto {
  @IsString()
  @IsNotEmpty()
  titulo: string;
  @IsDateString()
  @IsNotEmpty()
  fechaPublicacion: Date;
  @IsString()
  lugar: string;
  @IsString()
  autor: string;
  @IsString()
  contenido: string;
}

export class UpdateNoticiaDto {
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  titulo?: string;
  @IsDateString()
  @IsNotEmpty()
  @IsOptional()
  fechaPublicacion?: Date;
  @IsString()
  @IsOptional()
  lugar?: string;
  @IsString()
  @IsOptional()
  autor?: string;
  @IsString()
  @IsOptional()
  contenido?: string;
}
