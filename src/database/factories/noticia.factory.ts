import {
  randCatchPhrase,
  randCity,
  randFullName,
  randParagraph,
} from '@ngneat/falso';
import { Noticia } from '../../noticias/noticias.entity';
import { define } from '@paranode/typeorm-seeding';

define(Noticia, () => {
  const noticia = new Noticia();
  noticia.titulo = randCatchPhrase();
  //noticia.fechaPublicacion = new Date();
  noticia.lugar = randCity();
  noticia.autor = randFullName();
  noticia.contenido = randParagraph();
  return noticia;
});
