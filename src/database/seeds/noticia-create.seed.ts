import { Noticia } from '../../noticias/noticias.entity';
import { Factory, Seeder } from '@paranode/typeorm-seeding';

export default class NoticiaCreateSeed implements Seeder {
  public async run(factory: Factory): Promise<any> {
    await factory(Noticia)().createMany(20);
  }
}
