import { Module } from '@nestjs/common';
import { NoticiasModule } from './noticias/noticias.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from '../database/datasource';

@Module({
  imports: [NoticiasModule, TypeOrmModule.forRoot(dataSourceOptions)],
  controllers: [],
  providers: [],
})
export class AppModule {}
